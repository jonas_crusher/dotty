
source /usr/local/etc/bash_completion.d/git-completion.bash
source /usr/local/etc/bash_completion.d/git-prompt.sh

function setup_prompts_for_git
{
  local      BLUE="\[\e[0;34m\]"
  local       RED="\[\e[38;5;208m\]"
  local    YELLOW="\[\e[0;33m\]"
  local    PURPLE="\[\e[0;35m\]"
  local CLR_RESET="\[\e[0m\]"
  case $TERM in
    xterm*)
    TITLEBAR='\[\033]0;\u@\h:\w\007\]'
    ;;
    *)
    TITLEBAR=""
    ;;
  esac

  PS1="${TITLEBAR}$BLUE[$RED\$(date +%H:%M)$BLUE][$RED\u@\h:$PURPLE\w$YELLOW\$(__git_ps1)$BLUE]$CLR_RESET\n\$ "
  PS2='> '
  PS4='+ '
}

function gitroot
{
    cd `git root`
}

# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'

export EDITOR=/usr/bin/vim

setup_prompts_for_git

alias vim='rvm system do /usr/local/bin/vim $@'
alias ls='ls -Gp'

export ANDROID_HOME=/usr/local/var/lib/android-sdk/
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
export PATH="$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$GOPATH:$PATH"
export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8

#share history
shopt -s histappend
export HISTSIZE=100000
export HISTFILESIZE=100000
export HISTCONTROL=ignoredups:erasedups
export PROMPT_COMMAND="history -a;history -c;history -r;$PROMPT_COMMAND"

alias unfuck='reset; stty sane; tput rs1; echo -e "\033c"'

eval "$(thefuck --alias)"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
