### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

shopt -s histappend
export HISTSIZE=100000
export HISTFILESIZE=100000
export HISTCONTROL=ignoredups:erasedups
export PROMPT_COMMAND="history -a;history -c;history -r;$PROMPT_COMMAND"


alias ls="ls -Gp"

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

export PATH="$HOME/.rvm/bin:$PATH" # Add RVM to PATH for scripting
